package au.edu.unimelb.cis.ds;

import au.edu.unimelb.cis.ds.commands.filesync.InstructionCommand;
import au.edu.unimelb.cis.ds.commands.filesync.InstructionCommandFactory;
import au.edu.unimelb.cis.ds.transformers.Deserializer;
import au.edu.unimelb.cis.ds.transformers.RRCommandDeserializer;
import au.edu.unimelb.cis.ds.transformers.RRCommandSerializer;
import au.edu.unimelb.cis.ds.transformers.Serializer;
import filesync.Block;
import filesync.Instruction;
import filesync.NewBlockInstruction;
import filesync.StartUpdateInstruction;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class TransformerTest {

    private final Logger log = Logger.getLogger(this.getClass());

    private static HashMap<String, Object> context;
    private static InstructionCommandFactory factory;
    private static RRCommandSerializer serializer;
    private static RRCommandDeserializer deserializer;

    @BeforeClass
    public static void setUp() {
        context = new HashMap<String, Object>();
        HashMap<String, Object> globalContext = new HashMap <String, Object>();
        globalContext.put("some_dependency", "testing");
        factory = new InstructionCommandFactory(globalContext);
        serializer = new RRCommandSerializer();
        deserializer = new RRCommandDeserializer(factory);
    }

    @Test
    public void test1() throws Serializer.SerializerException, Deserializer.DeserializerException {
        Instruction instruction = new StartUpdateInstruction();
        InstructionCommand commandIn = factory.createFromInstruction(instruction);

        String jsonOne = serializer.serialize(commandIn);
        log.debug("First Serialize: " + jsonOne);
        assertFalse(jsonOne.isEmpty());

        InstructionCommand commandOut = (InstructionCommand) deserializer.deserialize(jsonOne);
        assertNotNull(commandOut);

        String jsonTwo = serializer.serialize(commandIn);
        log.debug("Second Serialize: " + jsonTwo);
        assertFalse(jsonTwo.isEmpty());

        assertEquals(jsonOne, jsonTwo);
    }

    @Test
    public void test2() throws Serializer.SerializerException, Deserializer.DeserializerException {
        Block block = new Block();
        block.setBytes(new byte[]{1,2,3});
        block.setHash("somehash");
        block.setLength(3);
        Instruction instruction = new NewBlockInstruction(block);
        InstructionCommand commandIn = factory.createFromInstruction(instruction);

        String jsonOne = serializer.serialize(commandIn);
        log.debug("First Serialize: " + jsonOne);
        assertFalse(jsonOne.isEmpty());

        InstructionCommand commandOut = (InstructionCommand) deserializer.deserialize(jsonOne);
        assertNotNull(commandOut);

        String jsonTwo = serializer.serialize(commandIn);
        log.debug("Second Serialize: " + jsonTwo);
        assertFalse(jsonTwo.isEmpty());

        assertEquals(jsonOne, jsonTwo);
    }

}
