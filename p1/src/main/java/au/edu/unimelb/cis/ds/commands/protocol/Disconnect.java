package au.edu.unimelb.cis.ds.commands.protocol;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;

/**
 * Informs that an endpoint
 * has initiated the closing
 * of the conversation connection.
 */
public class Disconnect extends ProtocolCommand {

    public Disconnect() {
        super();
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {
        conversationState.endConversationAbruptly();
        return null;
    }

}
