package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.commands.protocol.Acknowledge;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import filesync.BlockUnavailableException;
import filesync.StartUpdateInstruction;

import java.io.IOException;

public class StartUpdateCommand extends InstructionCommand {

    private StartUpdateInstruction instruction;

    private StartUpdateInstruction getInstruction() {
        return instruction;
    }

    private void setInstruction(StartUpdateInstruction instruction) {
        this.instruction = instruction;
    }

    private StartUpdateCommand() {
        super();
    }

    public StartUpdateCommand(StartUpdateInstruction instruction) {
        this.instruction = instruction;
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {
        return defaultProcess(instruction, conversationState);
    }
}
