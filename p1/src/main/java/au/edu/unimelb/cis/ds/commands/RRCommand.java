package au.edu.unimelb.cis.ds.commands;

import flexjson.JSON;

import java.util.Map;

/**
 * Command that executes and always
 * returns another command afterwards.
 */
public interface RRCommand {

    /**
     * Executes the command
     * and returns another.
     *
     *
     * @param conversationState conversation state that the command executes under
     * @return response command
     */
    public RRCommand execute(RRConversationState conversationState);

    /**
     * Returns map of objects that the command can reference.
     *
     * Removes local context when serializing, it's repopulated at the other
     * end-point.
     *
     * @return dependency map
     */
    @JSON(include=false)
    public Map<String, Object> getCommandContext();

    /**
     * Sets map of objects that the command can reference
     *
     * @param commandContext
     */
    public void setCommandContext(Map<String, Object> commandContext);

}
