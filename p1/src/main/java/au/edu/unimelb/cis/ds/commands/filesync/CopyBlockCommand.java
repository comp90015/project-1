package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.commands.protocol.Acknowledge;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import filesync.BlockUnavailableException;
import filesync.CopyBlockInstruction;

import java.io.IOException;

public class CopyBlockCommand extends InstructionCommand {

    private CopyBlockInstruction instruction;

    public CopyBlockInstruction getInstruction() {
        return instruction;
    }

    private void setInstruction(CopyBlockInstruction instruction) {
        this.instruction = instruction;
    }

    private CopyBlockCommand() {
        super();
    }

    public CopyBlockCommand(CopyBlockInstruction instruction) {
        super();
        this.instruction = instruction;
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {

        // Get model
        FileSyncModel model = (FileSyncModel) getCommandContext().get("fileSyncModel");

        try {

            // Process instruction
            model.processInstruction(instruction);

        } catch (IOException e) {
            conversationState.endConversationAbruptly();
            return null;
        } catch (BlockUnavailableException e) {

            // Request a new block since we don't have it
            return new RequestNewBlockCommand(instruction);

        }

        // Acknowledge if all is well
        return conversationState.endConversation();
    }
}
