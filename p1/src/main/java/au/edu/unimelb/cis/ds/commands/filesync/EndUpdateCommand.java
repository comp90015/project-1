package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.commands.protocol.Acknowledge;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import filesync.BlockUnavailableException;
import filesync.EndUpdateInstruction;

import java.io.IOException;

public class EndUpdateCommand extends InstructionCommand {

    private EndUpdateInstruction instruction;

    private EndUpdateInstruction getInstruction() {
        return instruction;
    }

    private void setInstruction(EndUpdateInstruction instruction) {
        this.instruction = instruction;
    }

    private EndUpdateCommand() {
        super();
    }

    public EndUpdateCommand(EndUpdateInstruction instruction) {
        super();
        this.instruction = instruction;
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {
        return defaultProcess(instruction, conversationState);
    }
}
