package au.edu.unimelb.cis.ds.transformers;

/**
 * Serializer for messages that must be dispatched
 * to other systems.
 */
public interface Serializer {

    public class SerializerException extends Exception {
        public SerializerException(String message) {
            super(message);
        }
        public SerializerException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Serializes the given object.
     */
    public String serialize(Object object) throws SerializerException;
}
