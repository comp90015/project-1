package au.edu.unimelb.cis.ds.connections;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Provides an n-channel connection handler
 * where each channel has a Request-Reply protocol.
 */
public interface NChannelConnHandler {

    /**
     * Reads message from given channel.
     *
     *
     * @param channelNumber which channel
     * @return message
     * @throws IOException
     */
    String read(int channelNumber) throws IOException;

    /**
     * Writes message to given channel
     *
     *
     * @param channelNumber which channel
     * @param message message to send
     * @return self
     * @throws IOException
     */
    NChannelConnHandler write(int channelNumber, String message) throws IOException;

    /**
     * Listens for a connection on provided address.
     * Channels occupy two sockets: port number , port number + 1.
     *
     * @param inetAddress net address
     * @param portNumber minimum of channel port numbers
     * @return self
     */
    NChannelConnHandler listenForConnection(InetAddress inetAddress, int portNumber) throws IOException;

    /**
     * Connects to provided address.
     * Channels occupy two sockets: port number , port number + 1.
     *
     * @param inetAddress net address
     * @param portNumber minimum of channel port numbers
     * @return self
     */
    NChannelConnHandler connect(InetAddress inetAddress, int portNumber) throws IOException;


    /**
     * Idempotent close operation.
     *
     * @return self
     */
    NChannelConnHandler closeConnection();
}
