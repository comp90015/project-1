package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRCommandFactory;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import filesync.BlockUnavailableException;
import filesync.CopyBlockInstruction;
import filesync.Instruction;
import filesync.NewBlockInstruction;

import java.io.IOException;
import java.util.List;

public class RequestNewBlockCommand extends InstructionCommand {

    private CopyBlockInstruction instruction;

    private CopyBlockInstruction getInstruction() {
        return instruction;
    }

    private void setInstruction(CopyBlockInstruction instruction) {
        this.instruction = instruction;
    }

    private RequestNewBlockCommand() {
        super();
    }

    public RequestNewBlockCommand(CopyBlockInstruction instruction) {
        this.instruction = instruction;
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {
        // Find the last copy command from history to obtain the bytes
        List<RRCommand> history = conversationState.getCommandHistory();
        if (history.isEmpty()) {
            throw new RuntimeException("Expected history not to be empty");
        } else {
            RRCommand command = history.get(history.size() - 1);
            if (command instanceof CopyBlockCommand) {
                CopyBlockCommand copy = (CopyBlockCommand) command;
                // Send an upgraded instruction command
                NewBlockInstruction upgraded = new NewBlockInstruction(copy.getInstruction());
                return new NewBlockCommand(upgraded);
            } else {
                throw new RuntimeException("Expected last command to be copy");
            }
        }
    }
}
