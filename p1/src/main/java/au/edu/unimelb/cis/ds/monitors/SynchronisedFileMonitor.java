package au.edu.unimelb.cis.ds.monitors;

import au.edu.unimelb.cis.ds.filesync.ThreadSafeSynchronisedFile;
import filesync.SynchronisedFile;
import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Reports changes in file to SynchronisedFile
 */
public class SynchronisedFileMonitor {
    private final Logger log = Logger.getLogger(this.getClass());

    private ThreadSafeSynchronisedFile synchronisedFile;

    private Thread monitoringFileThread;
    private boolean monitoring;

    public SynchronisedFileMonitor(ThreadSafeSynchronisedFile synchronisedFile) {
        this.synchronisedFile = synchronisedFile;
    }

    /**
     * Starts asynchronously monitoring file for changes
     */
    public void startMonitoringFile() {

        // Error if already running
        if (isMonitoring()) {
            throw new RuntimeException("Already monitoring file.");
        }

        // Create monitoring thread as daemon
        log.debug("Creating monitoring thread");
        PollingMonitorRunnable pollingMonitorRunnable = new PollingMonitorRunnable();
        monitoringFileThread = new Thread(pollingMonitorRunnable);
        monitoringFileThread.setDaemon(true);

        // Start monitoring
        monitoringFileThread.start();
        monitoring = true;
        log.debug("Started monitoring thread");

    }

    /**
     * Checks if file monitoring is occurring
     * @return true if monitoring
     */
    public boolean isMonitoring() {

        // Check if monitoring file
        return monitoring;

    }

    /**
     * Philosophically we should allow the cessation of
     * monitoring a file. However since we do not
     * have control of the existing SynchronisedFile, we
     * cannot safely exit due its non transactional AND blocking
     * nature. As well, the resources are not designed
     * to be released with interrupts.
     * <p/>
     * This is however here, for later when the SynchronisedFile
     * may later be non-blocking/modifiable.
     *
     * @throws org.apache.commons.lang.NotImplementedException
     *
     */
    public void stopMonitoringFile() {
        throw new NotImplementedException();
    }

    /**
     * Actual monitoring code to be run
     */
    private class PollingMonitorRunnable implements Runnable {
        /**
         * Runs forever, polling for file changes via
         * the respective SynchronisedFile
         */
        @Override
        public void run() {

            // Run forever
            while (true) {
                try {

                    // Sleep 3 seconds
                    log.debug("Sleeping");
                    TimeUnit.SECONDS.sleep(3);

                    // Update SyncFile for changes
                    log.debug("Checking for changes");
                    synchronisedFile.checkFileState();

                } catch (IOException e) {
                    log.error("Error checking file state", e);
                } catch (InterruptedException e) {
                    log.error("Interrupted", e);
                }
            }
        }
    }

}
