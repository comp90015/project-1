package au.edu.unimelb.cis.ds.filesync;

/**
 * End point role
 * for file synchronisation
 */
public enum FileSyncEndPointRole {
    /**
     * End point acting as source
     */
    SOURCE,
    /**
     * End point acting as destination
     */
    DESTINATION,
    /**
     * End point can act as either source or destination
     * Occurs before uni-directional
     * or duplex file synchronisation
     */
    ANY;

    /**
     * Returns complement role
     * NOTE: ANY complement is ANY
     *
     * @param endPointRole end point role
     * @return complement of role
     */
    public static FileSyncEndPointRole getComplement(FileSyncEndPointRole endPointRole) {
        if (endPointRole == DESTINATION) {
            return SOURCE;
        } else if (endPointRole == SOURCE) {
            return DESTINATION;
        } else {
            return ANY;
        }
    }
}
