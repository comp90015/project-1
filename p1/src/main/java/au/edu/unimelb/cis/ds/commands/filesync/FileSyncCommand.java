package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRAbstractCommand;

/**
 * FileSync commands
 */
public abstract class FileSyncCommand extends RRAbstractCommand {

    protected FileSyncCommand() {
        super();

        // Require these unresolved dependencies
        getCommandContext().put("fileSyncModel", null);

    }
}
