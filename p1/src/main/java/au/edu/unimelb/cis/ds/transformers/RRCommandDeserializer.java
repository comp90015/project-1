package au.edu.unimelb.cis.ds.transformers;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRCommandFactory;
import filesync.Instruction;
import filesync.InstructionFactory;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.ObjectBinder;
import flexjson.ObjectFactory;

import java.lang.reflect.Type;

public class RRCommandDeserializer implements Deserializer<RRCommand> {

    private JSONDeserializer<RRCommand> deserializer =
            new JSONDeserializer<RRCommand>();
    private JSONSerializer serializer = new JSONSerializer();
    private RRCommandFactory commandFactory;
    private InstructionFactory instructionFactory;

    public RRCommandDeserializer(RRCommandFactory factory) {
        commandFactory = factory;
        instructionFactory = new InstructionFactory();
        deserializer.use(Instruction.class, new ObjectFactory() {
            @Override
            public Instruction instantiate(ObjectBinder context, Object value,
                                           Type targetType, Class targetClass) {
                return instructionFactory.FromJSON(serializer.serialize(value));
            }
        });
    }

    @Override
    public RRCommand deserialize(String string) throws DeserializerException {
        RRCommand command = deserializer.deserialize(string);
        commandFactory.injectDependencies(command);
        return command;
    }
}
