package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommandFactory;
import filesync.*;

import java.util.Map;

public class InstructionCommandFactory extends RRCommandFactory {

    public InstructionCommandFactory(Map<String, Object> globalContext) {
        super(globalContext);
    }

    /**
     * Creates command from file sync instruction provided
     * by @link{filesync.SynchronisedFile}
     *
     * @param instruction instruction
     * @return command representing instruction
     */
    public InstructionCommand createFromInstruction(Instruction instruction) {

        // Create correct command according to concrete instruction
        if (instruction instanceof StartUpdateInstruction) {
            return createStartUpdateCommand((StartUpdateInstruction) instruction);
        } else if (instruction instanceof CopyBlockInstruction) {
            return createCopyBlockCommand((CopyBlockInstruction) instruction);
        } else if (instruction instanceof NewBlockInstruction) {
            return createNewBlockCommand((NewBlockInstruction) instruction);
        } else if (instruction instanceof EndUpdateInstruction) {
            return createEndUpdateCommand((EndUpdateInstruction) instruction);
        } else {
            throw new RuntimeException("Instruction not accounted for");
        }

    }

    //// Instruction commands

    private StartUpdateCommand createStartUpdateCommand(StartUpdateInstruction instruction) {
        return new StartUpdateCommand(instruction);
    }

    private CopyBlockCommand createCopyBlockCommand(CopyBlockInstruction instruction) {
        return new CopyBlockCommand(instruction);
    }

    private NewBlockCommand createNewBlockCommand(NewBlockInstruction instruction) {
        return new NewBlockCommand(instruction);
    }

    private EndUpdateCommand createEndUpdateCommand(EndUpdateInstruction instruction) {
        return new EndUpdateCommand(instruction);
    }
}