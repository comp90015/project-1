package au.edu.unimelb.cis.ds.filesync;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Options for an end point in file sync
 * communication.
 *
 */
public class FileSyncEndPointOptions {
    private int maximumBlockSize;
    private FileSyncEndPointRole requiredRole;

    private FileSyncEndPointOptions() {
    }

    /**
     * Creates options for end point
     * @param maximumBlockSize maximum block size end point can endure
     * @param requiredRole required role end point
     *                     (if ANY, then can be either source
     *                     or destination after negotiation)
     */
    public FileSyncEndPointOptions(int maximumBlockSize, FileSyncEndPointRole requiredRole) {
        checkArgument(requiredRole != null);
        checkArgument(maximumBlockSize > 0, "Argument was %s, but expected positive", maximumBlockSize);
        this.maximumBlockSize = maximumBlockSize;
        this.requiredRole = requiredRole;
    }

    /**
     * Checks if compromised options can be built to satisfy
     * both endpoint's needs
     * @param otherEndOptions other endpoint's options
     * @return true if possible
     */
    public boolean canBuildAgreeableOptions(FileSyncEndPointOptions otherEndOptions) {
        checkArgument(otherEndOptions != null);

        // Return true if roles are unequal or one is set to ANY
        FileSyncEndPointRole otherRequiredRole = otherEndOptions.requiredRole;
        boolean bothRolesNotEqualOrEitherRoleIsAny =
                !requiredRole.equals(otherRequiredRole) || requiredRole.equals(FileSyncEndPointRole.ANY);
        return bothRolesNotEqualOrEitherRoleIsAny;

    }

    /**
     * Builds compromised options from self that are workable
     * with the other end point's options.
     *
     * It takes the minimum of the
     * two endpoint's maximum block sizes.
     * It fails if both end point's required roles
     * are the same and specific (e.g. both want to be source).
     * If both end point's roles are set to ANY,
     * then self will assign itself as DESTINATION
     *
     * @param otherEndPointOptions other endpoint's options
     * @return compromised options if possible
     * @throws FileSyncNegotiationFailedException
     *
     */
    public FileSyncEndPointOptions buildAgreeableOptions(
            FileSyncEndPointOptions otherEndPointOptions) throws FileSyncNegotiationFailedException {
        checkArgument(otherEndPointOptions != null);

        // Set new block size to minimum of end point's max block sizes
        int otherMaxBlockSize = otherEndPointOptions.maximumBlockSize;
        int newMaximumBlockSize = Math.min(maximumBlockSize, otherMaxBlockSize);

        // If both endpoint's required roles are the same
        FileSyncEndPointRole newRequiredRole;
        FileSyncEndPointRole otherRequiredRole = otherEndPointOptions.requiredRole;
        if (requiredRole.equals(otherRequiredRole)) {

            // If both are ANY, set self to DESTINATION
            if (requiredRole == FileSyncEndPointRole.ANY) {
                newRequiredRole = FileSyncEndPointRole.DESTINATION;
            }
            // Otherwise, we're in a disagreeable state, end the talks.
            else {
                throw new FileSyncNegotiationFailedException();
            }
        }

        // If own's role is set to ANY, assign complement of other end point's
        if (requiredRole == FileSyncEndPointRole.ANY)
        {
            newRequiredRole = FileSyncEndPointRole.getComplement(otherRequiredRole);
        }
        // Otherwise, keep own role
        else {
            newRequiredRole = requiredRole;
        }

        return new FileSyncEndPointOptions(newMaximumBlockSize, newRequiredRole);
    }

    /**
     * Returns maximum block size
     * @return maximum block size
     */
    public int getMaximumBlockSize() {
        return maximumBlockSize;
    }

    /**
     * Returns required role
     * @return required role
     */
    public FileSyncEndPointRole getRequiredRole() {
        return requiredRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileSyncEndPointOptions that = (FileSyncEndPointOptions) o;

        if (maximumBlockSize != that.maximumBlockSize) return false;
        if (requiredRole != that.requiredRole) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = maximumBlockSize;
        result = 31 * result + requiredRole.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FileSyncEndPointOptions{" +
                "maximumBlockSize=" + maximumBlockSize +
                ", requiredRole=" + requiredRole +
                '}';
    }
}
