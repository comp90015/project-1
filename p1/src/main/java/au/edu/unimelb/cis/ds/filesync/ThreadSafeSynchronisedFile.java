package au.edu.unimelb.cis.ds.filesync;

import filesync.BlockUnavailableException;
import filesync.Instruction;
import filesync.SynchronisedFile;

import java.io.IOException;

public class ThreadSafeSynchronisedFile {

    public ThreadSafeSynchronisedFile(SynchronisedFile synchronisedFile) {
        this.synchronisedFile = synchronisedFile;
    }

    private SynchronisedFile synchronisedFile;

    // Synchronised not required, nextInstruction in synchronisedFile is thread-safe
    public Instruction nextInstruction()
    {
        return synchronisedFile.NextInstruction();
    }

    synchronized public void processInstruction(Instruction instruction) throws IOException, BlockUnavailableException {
        synchronisedFile.ProcessInstruction(instruction);
    }

    synchronized public void checkFileState() throws IOException, InterruptedException {
        synchronisedFile.CheckFileState();
    }
}
