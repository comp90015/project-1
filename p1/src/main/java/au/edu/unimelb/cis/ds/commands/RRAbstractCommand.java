package au.edu.unimelb.cis.ds.commands;

import flexjson.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * An abstract implementation of an {@link RRCommand}
 * that provides a simple way of keeping context.
 */
public abstract class RRAbstractCommand implements RRCommand {

    /**
     * Holds objects that a command can reference.
     */
    private Map<String, Object> commandContext;

    public RRAbstractCommand() {

        // Require these unresolved dependencies
        this.commandContext = new HashMap<String, Object>();

    }

    @Override
    @JSON(include = false)
    public Map<String, Object> getCommandContext() {
        return commandContext;
    }

    @Override
    public void setCommandContext(Map<String, Object> commandContext) {
        this.commandContext = commandContext;
    }
}
