package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.commands.protocol.Acknowledge;
import au.edu.unimelb.cis.ds.commands.protocol.Disconnect;
import au.edu.unimelb.cis.ds.filesync.FileSyncEndPointOptions;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import au.edu.unimelb.cis.ds.filesync.FileSyncNegotiationFailedException;
import org.apache.log4j.Logger;

/**
 * Negotiation between two end points
 * around file synchronisation.
 */
public class NegotiateFileSyncOptions extends FileSyncCommand {
    private final Logger log = Logger.getLogger(this.getClass());

    /**
     * Other options, in the eyes of the endpoint that
     * executes the command.
     */
    private FileSyncEndPointOptions otherOptions;

    private FileSyncEndPointOptions getOtherOptions() {
        return otherOptions;
    }

    private void setOtherOptions(FileSyncEndPointOptions otherOptions) {
        this.otherOptions = otherOptions;
    }

    private NegotiateFileSyncOptions() {
        super();
    }

    /**
     * Create command with own options sent to be executed.
     *
     * @param ownOptions own endpoint's options
     */
    public NegotiateFileSyncOptions(FileSyncEndPointOptions ownOptions) {
        super();
        this.otherOptions = ownOptions;
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {

        // Get own options
        FileSyncModel ownFileSyncModel =
                (FileSyncModel) getCommandContext().get("fileSyncModel");
        FileSyncEndPointOptions ownOptions = ownFileSyncModel.getFileSyncOptions();
        log.trace(String.format("Own filesync options: %s", ownOptions));
        log.trace(String.format("Other filesync options: %s", otherOptions));

        FileSyncEndPointOptions ownCompromisedOptions = null;
        try {

            // If own options can be compromised to meet other
            if (ownOptions.canBuildAgreeableOptions(otherOptions)) {

                // Build compromised option
                ownCompromisedOptions = ownOptions.buildAgreeableOptions(otherOptions);
                log.trace(String.format("Own compromised filesync options: %s", ownCompromisedOptions));

                // If compromised is same as own option, end the conversation, negotiation is complete.
                if (ownOptions.equals(ownCompromisedOptions)) {
                    log.debug("Compromised same as own, acknowledging");
                    return conversationState.endConversation();
                }

                // Set own options to compromised version
                ownFileSyncModel.setFileSyncOptions(ownCompromisedOptions);
                log.debug(String.format("Set own filesync options to compromised: %s"
                        , ownFileSyncModel.getFileSyncOptions()));

            }
            // Otherwise negotiation failed, end the connection
            else {

                conversationState.endConversationAbruptly();
                return conversationState.endConversationAbruptly();

            }

        } catch (FileSyncNegotiationFailedException e) {
            // You should never reach here, if so then there is a bug.
            // Luckily we can recover by ending the connection.
            log.error("Unexpected disagreement. Try checking first.", e);

            return conversationState.endConversationAbruptly();
        }

        // Send own now compromised options to other
        // endpoint for further negotiation
        log.debug("Sending back own compromised filesync options");
        return new NegotiateFileSyncOptions(ownCompromisedOptions);

    }

}
