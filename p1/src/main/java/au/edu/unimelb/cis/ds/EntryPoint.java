package au.edu.unimelb.cis.ds;

import au.edu.unimelb.cis.ds.controllers.AppController;
import au.edu.unimelb.cis.ds.filesync.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Run as client by having one argument: filename
 * Run as server by having two arguments: filename hostname
 */
public class EntryPoint
{
    private static final Logger log = Logger.getLogger(EntryPoint.class);

    public static void main( String[] args )
    {
        final int BlockSize = 1024;
        final int PortNumber = 4321;

        boolean isServer = false;
        String hostname = null;
        String filename = null;

        // If args only has filename, then we are the server
        if (args.length == 1) {
            isServer = true;
            filename = args[0];
            hostname = "localhost";
        }
        // If args has filename and hostname, then we are the client
        else if (args.length == 2) {
            isServer = false;
            hostname = args[0];
            filename = args[1];
        }
        // Otherwise, we can't handle this, time to die
        else {
            System.err.println("Arguments must be of form 'filename' or" +
                    " 'hostname filename'");
            System.exit(-1);
        }


        //// Build and run application

        // If server
        FileSyncEndPointOptions options;
        if (isServer) {

            // We want to be destination
            options = new FileSyncEndPointOptions(BlockSize,
                    FileSyncEndPointRole.DESTINATION);

        }
        // Otherwise as client
        else {

            // We want to be source
            options = new FileSyncEndPointOptions(BlockSize,
                    FileSyncEndPointRole.SOURCE);

        }

        // Create file sync model
        FileSyncModelFactory fileSyncModelFactory = new FileSyncModelFactory();
        FileSyncModel fileSyncModel = null;
        try {
            fileSyncModel = fileSyncModelFactory.createFileSyncModel(filename, options);
        } catch (IOException e) {
            System.err.printf("Cannot access file: %s", filename);
            System.exit(-1);
        }

        // Create app controller
        AppController appController = new AppController(fileSyncModel);

        // Run app controller
        try {

            // If server
            if (isServer) {

                // Listen and sync
                appController.listenThenConstantlySync(InetAddress.getByName(hostname), PortNumber);

            }
            // Otherwise as client
            else {

                // Connect and sync
                appController.connectThenConstantlySync(InetAddress.getByName(hostname), PortNumber);

            }

        }
        catch (UnknownHostException e) {
            System.err.println("Cannot get own hostname!");
            System.exit(-1);
        } catch (IOException e) {
            System.err.println("Network error");
            e.printStackTrace();
            System.exit(-1);
        } catch (FileSyncNegotiationFailedException e) {
            System.err.println("Negotiations failed for file sync options");
            System.exit(-1);
        }
    }
}
