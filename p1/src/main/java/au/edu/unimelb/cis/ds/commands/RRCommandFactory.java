package au.edu.unimelb.cis.ds.commands;

import au.edu.unimelb.cis.ds.commands.filesync.*;
import filesync.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Creates commands and
 * associates dependencies required.
 */
public class RRCommandFactory {

    // All objects that commands can select from
    private Map<String, Object> globalContext;

    /**
     * Creates command factory
     *
     * @param globalContext The global set of dependencies this factory can
     *                      inject into {@link RRCommand}.
     */
    public RRCommandFactory(Map<String, Object> globalContext) {
        this.globalContext = globalContext;
    }

    /**
     * @param command Inject dependencies from the global context into this
     * {@link RRCommand}.
     */
    public void injectDependencies(RRCommand command) {

        // Get command context
        Map<String, Object> commandContext = command.getCommandContext();

        // For each unresolved dependency in command context
        for (String dependencyName : commandContext.keySet()) {

            Object dependencyObject = commandContext.get(dependencyName);

            // If dep. object null, then unresolved
            if (dependencyObject == null) {

                // Resolve
                commandContext.put(dependencyName, this.globalContext.get(dependencyName));

            }

        }
    }
}
