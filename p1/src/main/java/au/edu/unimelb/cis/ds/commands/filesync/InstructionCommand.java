package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.commands.protocol.Acknowledge;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import filesync.BlockUnavailableException;
import filesync.Instruction;

import java.io.IOException;

/**
 * Command that encapsulates {@link filesync.Instruction}.
 */
public abstract class InstructionCommand extends FileSyncCommand {

    protected InstructionCommand() {
        super();
    }

    /**
     * The default method to process an instruction
     * @param conversationState conversate state
     * @return response command
     */
    public RRCommand defaultProcess(Instruction instruction, RRConversationState conversationState) {

        // Get model
        FileSyncModel model = (FileSyncModel) getCommandContext().get("fileSyncModel");

        try {

            // Process
            model.processInstruction(instruction);

        } catch (IOException e) {
            return conversationState.endConversationAbruptly();
        } catch (BlockUnavailableException e) {
            throw new RuntimeException("Unexpected exception", e);
        }

        // Acknowledge if all is well
        return conversationState.endConversation();
    }
}
