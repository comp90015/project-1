package au.edu.unimelb.cis.ds.connections;

/**
 * Protocol Message passed through
 * connection stream.
 */
public class ChannelProtocolMessage {
    private int channelNumber;
    private String message;

    private ChannelProtocolMessage() {
    }

    public ChannelProtocolMessage(int channelNumber, String message) {
        this.channelNumber = channelNumber;
        this.message = message;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setChannelNumber(int channelNumber) {
        this.channelNumber = channelNumber;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ChannelProtocolMessage{" +
                "channelNumber=" + channelNumber +
                ", message='" + message + '\'' +
                '}';
    }
}
