package au.edu.unimelb.cis.ds.connections;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRCommandFactory;
import au.edu.unimelb.cis.ds.commands.RRConversation;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.transformers.Deserializer;
import au.edu.unimelb.cis.ds.transformers.RRCommandDeserializer;
import au.edu.unimelb.cis.ds.transformers.RRCommandSerializer;
import au.edu.unimelb.cis.ds.transformers.Serializer;

import java.io.IOException;

/**
 * Wrapper class around ConnChannelHandler that allows
 * operations for RRConversations.
 */
public class RRConnChannelOperator {

    // Command related fields
    private final RRCommandDeserializer commandDeserializer;
    private final RRCommandSerializer commandSerializer;
    private final RRCommandFactory commandFactory;

    public RRConnChannelOperator(RRCommandFactory commandFactory) {
        // Set factory
        this.commandFactory = commandFactory;

        // Use RRCommand serialisation
        commandDeserializer = new RRCommandDeserializer(commandFactory);
        commandSerializer = new RRCommandSerializer();

    }

    /**
     * Sends command to channel in connection
     *
     * @param channelNumber channel
     * @param connHandler connection handler
     * @param ownRequestCommand command to send
     * @throws IOException
     */
    private void sendCommand(int channelNumber, NChannelConnHandler connHandler, RRCommand ownRequestCommand) throws IOException {

        String ownRequestMessage = null;
        try {
            // Serialise
            ownRequestMessage = commandSerializer.serialize(ownRequestCommand);

        } catch (Serializer.SerializerException e) {
            // Programmatic error
            throw new RuntimeException("Serialisation error", e);
        }

        // Write to channel
        connHandler.write(channelNumber, ownRequestMessage);
    }

    /**
     * Receives command from channel in connection
     * @param channelNumber channel
     * @param connHandler connection handler
     * @return command from channel
     * @throws IOException
     */
    private RRCommand receiveCommand(int channelNumber, NChannelConnHandler connHandler) throws IOException {

        // Read response command from other endpoint
        String otherResponseMessage = connHandler.read(channelNumber);
        RRCommand receivedCommand = null;

        try {

            // Deserialise
            receivedCommand = commandDeserializer.deserialize(otherResponseMessage);

            // Inject dependencies
            commandFactory.injectDependencies(receivedCommand);

        } catch (Deserializer.DeserializerException e) {

            // Programmatic error
            new RuntimeException("Deserialisation error", e);

        }

        // Return command
        return receivedCommand;
    }

    /**
     * Creates and runs a conversation
     * between a channel
     * with the initial command.
     *
     * @param channelNumber which channel number
     * @param connHandler connection handler
     * @param initialCommand initial command to send to other endpoint
     * @return conversation after it has run
     * @throws java.io.IOException
     */
    public RRConversation converse(
            int channelNumber,
            NChannelConnHandler connHandler,
            RRCommand initialCommand) throws IOException {

        // Set own request to initial
        RRCommand ownRequestCommand = initialCommand;

        // Create new conversation
        RRConversation conversation = new RRConversation(initialCommand);

        // Forever
        while (true) {

            // Send own request command to endpoint
            sendCommand(channelNumber, connHandler, ownRequestCommand);

            // Read response command from other endpoint
            RRCommand otherResponseCommand = receiveCommand(channelNumber, connHandler);

            // Execute other endpoint's response command
            // and find new own endpoint's request command
            ownRequestCommand = conversation.executeCommand(otherResponseCommand);

            // If conversation has officially ended
            RRConversationState conversationState = conversation.getConversationState();
            if (conversationState.isEnded()) {

                // If a response command exists
                if (ownRequestCommand != null) {

                    // Final send
                    sendCommand(channelNumber, connHandler, ownRequestCommand);

                }

                // Conversation is over
                break;
            }

        }

        // Return conversation
        return conversation;

    }

    /**
     * Waits for a conversation to be initiated
     * by other endpoint on the given channel
     * and then converses.
     *
     * @param channelNumber which channel number
     * @param connHandler connection handler
     * @return conversation after it has run
     * @throws java.io.IOException
     */
    public RRConversation listenThenConverse(
            int channelNumber,
            NChannelConnHandler connHandler) throws IOException {

        // Create new conversation
        RRConversation conversation = new RRConversation();

        while (true) {

            // Receive command from other
            RRCommand otherResponseCommand = receiveCommand(channelNumber, connHandler);

            // Execute other endpoint's response command
            // and find new own endpoint's request command
            RRCommand ownRequestCommand = conversation.executeCommand(otherResponseCommand);

            // If conversation has officially ended
            RRConversationState conversationState = conversation.getConversationState();
            if (conversationState.isEnded()) {

                // If a response command exists
                if (ownRequestCommand != null) {

                    // Final send
                    sendCommand(channelNumber, connHandler, ownRequestCommand);

                }

                // Conversation is over
                break;
            }

            // Send own request command to endpoint
            sendCommand(channelNumber, connHandler, ownRequestCommand);

        }

        // Return conversation
        return conversation;

    }
}
