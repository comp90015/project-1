package au.edu.unimelb.cis.ds.filesync;

/**
 * Exception thrown when two end point options
 * are not reconcilable
 */
public class FileSyncNegotiationFailedException extends Exception{
}