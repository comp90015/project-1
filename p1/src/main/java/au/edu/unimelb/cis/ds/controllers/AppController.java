package au.edu.unimelb.cis.ds.controllers;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRCommandFactory;
import au.edu.unimelb.cis.ds.commands.RRConversation;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.commands.filesync.InstructionCommandFactory;
import au.edu.unimelb.cis.ds.commands.filesync.NegotiateFileSyncOptions;
import au.edu.unimelb.cis.ds.connections.NChannelConnHandler;
import au.edu.unimelb.cis.ds.connections.OneWayNChannelConnHandler;
import au.edu.unimelb.cis.ds.connections.RRConnChannelOperator;
import au.edu.unimelb.cis.ds.filesync.FileSyncEndPointRole;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import au.edu.unimelb.cis.ds.filesync.FileSyncNegotiationFailedException;
import filesync.Instruction;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;

/**
 * Stateless Application Controller
 * Routes execution streams
 *
 * When an endpoint connects to another,
 * the server/listener sends the first message
 * and negotiation is communicated via
 * the server endpoint.
 */
public class AppController {
    private final Logger log = Logger.getLogger(this.getClass());

    // Constants
    private final int NumberOfChannels = 1;
    private final int ConversingChannel = 0;

    // Communication
    private NChannelConnHandler fileSyncConnHandler;
    private RRConnChannelOperator connOperator;

    // Models
    private final FileSyncModel fileSyncModel;

    // Command related fields
    private final RRCommandFactory commandFactory;
    private final InstructionCommandFactory instructionCommandFactory;

    public AppController(FileSyncModel fileSyncModel) {
        log.debug("Creating app controller");

        // Set fields from args
        this.fileSyncModel = fileSyncModel;

        // Create factories
        // Provide global context to RRCommandFactory
        HashMap<String, Object> globalContext = new HashMap <String, Object>();
        globalContext.put("fileSyncModel", this.fileSyncModel);
        commandFactory = new RRCommandFactory(globalContext);
        instructionCommandFactory = new InstructionCommandFactory(globalContext);

        // Setup conn channel operator
        connOperator = new RRConnChannelOperator(commandFactory);
    }

    /**
     *
     * Connects to an endpoint
     * and constantly synchronises file with it
     *
     * @param inetAddress internet address
     * @param portNumber port number
     * @throws IOException
     * @throws FileSyncNegotiationFailedException
     */
    public void connectThenConstantlySync(InetAddress inetAddress, Integer portNumber) throws IOException, FileSyncNegotiationFailedException {

        // Connect then build duplex connection
        fileSyncConnHandler = new OneWayNChannelConnHandler(NumberOfChannels);
        fileSyncConnHandler.connect(inetAddress, portNumber);

        // Listen and negotiate
        RRConversation negotiateConversation = connOperator.listenThenConverse(ConversingChannel, fileSyncConnHandler);

        // Return successful if the connection did not abruptly end
        RRConversationState conversationState = negotiateConversation.getConversationState();
        boolean negotiationSuccess = !conversationState.isAbruptEnding();

        // If negotiation failed
        if (!negotiationSuccess) {

            // Exit
            throw new FileSyncNegotiationFailedException();

        }

        // Forever sync
        foreverSync();

    }

    /**
     * Listens for a connection
     * and constantly synchronises file with
     * respective end point
     *
     * @param inetAddress internet address
     * @param portNumber port number
     * @throws IOException
     * @throws FileSyncNegotiationFailedException
     */
    public void listenThenConstantlySync(InetAddress inetAddress, Integer portNumber) throws IOException, FileSyncNegotiationFailedException {

        // Listen then build duplex connection
        fileSyncConnHandler = new OneWayNChannelConnHandler(NumberOfChannels);
        fileSyncConnHandler.listenForConnection(inetAddress, portNumber);

        // Negotiate options
        NegotiateFileSyncOptions negotiateCommand =
                new NegotiateFileSyncOptions(fileSyncModel.getFileSyncOptions());
        RRConversation negotiateConversation = connOperator.converse(ConversingChannel, fileSyncConnHandler, negotiateCommand);

        // Return successful if the connection did not abruptly end
        RRConversationState conversationState = negotiateConversation.getConversationState();
        boolean negotiationSuccess = !conversationState.isAbruptEnding();

        // If negotiation failed
        if (!negotiationSuccess) {

            // Exit
            throw new FileSyncNegotiationFailedException();

        }

        // Forever sync
        foreverSync();

    }


    private void foreverSync() throws IOException {

        // If acting as source, synchronise as source
        if (fileSyncModel.getFileSyncOptions().getRequiredRole() == FileSyncEndPointRole.SOURCE) {
            log.info("Acting as source");

            // Start file monitoring
            fileSyncModel.enableFileMonitoring();

            // Forever
            while (true) {

                // Get next instruction
                Instruction nextInstruction = fileSyncModel.getNextInstruction();

                // Build instruction command
                RRCommand instructionCommand = instructionCommandFactory.createFromInstruction(nextInstruction);

                // Converse with other endpoint
                RRConversation conversation =
                        connOperator.converse(ConversingChannel, fileSyncConnHandler, instructionCommand);

                // If conversation ended abruptly
                if (conversation.getConversationState().isAbruptEnding()) {
                    // Throw error, shouldn't happen.
                    new RuntimeException("Conversation ended abruptly in sync");
                }

            }
        }
        // Otherwise, synchronise as destination
        else {
            log.info("Acting as destination");
            while (true) {

                // Converse with other endpoint
                RRConversation conversation =
                        connOperator.listenThenConverse(ConversingChannel, fileSyncConnHandler);

                // If conversation ended abruptly
                if (conversation.getConversationState().isAbruptEnding()) {
                    // Throw error, shouldn't happen.
                    new RuntimeException("Conversation ended abruptly in sync");
                }

            }
        }
    }
}
