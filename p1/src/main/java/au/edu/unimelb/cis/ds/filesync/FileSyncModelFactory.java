package au.edu.unimelb.cis.ds.filesync;

import au.edu.unimelb.cis.ds.monitors.SynchronisedFileMonitor;
import filesync.SynchronisedFile;

import java.io.IOException;

/**
 * Creates file sync models
 */
public class FileSyncModelFactory {
    /**
     * Creates file sync model with batteries included.
     * Creation and startup of synchronised file handling
     * is covered.
     *
     * @param filename file to synchronise
     * @param options preferred options for endpoint filesync
     * @return file sync model
     * @throws IOException
     */
    public FileSyncModel createFileSyncModel(String filename, FileSyncEndPointOptions options) throws IOException {

        // Create SynchronisedFile for file
        SynchronisedFile synchronisedFile = new SynchronisedFile(filename);

        // Create thread-safe wrapper
        ThreadSafeSynchronisedFile threadSafeSynchronisedFile = new ThreadSafeSynchronisedFile(synchronisedFile);

        // Create monitor for file
        SynchronisedFileMonitor fileMonitor = new SynchronisedFileMonitor(threadSafeSynchronisedFile);

        // Create model itself
        FileSyncModel fileSyncModel = new FileSyncModel(options, threadSafeSynchronisedFile, fileMonitor);

        // Return file sync model
        return fileSyncModel;

    }
}
