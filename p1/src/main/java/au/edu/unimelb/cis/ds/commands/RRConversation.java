package au.edu.unimelb.cis.ds.commands;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents the conversation itself as seen
 * by own endpoint,
 * which in itself is a sequence
 * of RR commands across two endpoints.
 */
public class RRConversation {
    private RRConversationState conversationState;
    private List<RRCommand> commandHistory;

    public RRConversation() {
        commandHistory = new LinkedList<RRCommand>();
        conversationState = new RRConversationState(commandHistory);
    }

    public RRConversation(RRCommand initialCommand) {
        this();
        commandHistory.add(initialCommand);
    }

    /**
     * Returns history of commands including responses
     *
     * @return history of commands
     */
    public List<RRCommand> getCommandHistory() {
        return commandHistory;
    }

    /**
     * Returns state of conversation
     * @return state of conversation
     */
    public RRConversationState getConversationState() {
        return conversationState;
    }

    /**
     * Executes command and appends
     * itself and the generated response to the conversation.
     *
     * @param command target command to execute
     * @return response command
     */
    public RRCommand executeCommand(RRCommand command) {

        // Execute command
        RRCommand response = command.execute(conversationState);

        // Append command and its response to history
        commandHistory.add(command);
        commandHistory.add(response);

        // Return response command
        return response;
    }
}
