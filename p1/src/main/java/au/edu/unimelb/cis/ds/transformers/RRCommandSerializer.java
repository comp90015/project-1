package au.edu.unimelb.cis.ds.transformers;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import filesync.Instruction;
import flexjson.JSONSerializer;
import flexjson.transformer.AbstractTransformer;

public class RRCommandSerializer implements Serializer {

    private JSONSerializer serializer;

    public RRCommandSerializer() {
        serializer = new JSONSerializer();
        serializer.transform(new AbstractTransformer() {
            @Override
            public void transform(Object object) {
                if (object instanceof Instruction) {
                    Instruction instruction = (Instruction) object;
                    getContext().write(instruction.ToJSON());
                }
            }
        }, Instruction.class);
    }

    @Override
    public String serialize(Object object) throws SerializerException {
        if (object instanceof RRCommand) {
            RRCommand command = (RRCommand) object;
            return serializer.serialize(command);
        } else {
            throw new SerializerException("Object must be of class "
                    + RRCommand.class);
        }
    }
}
