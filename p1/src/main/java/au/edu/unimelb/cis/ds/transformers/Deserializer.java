package au.edu.unimelb.cis.ds.transformers;

/**
 * Deserializer for messages received form
 * form other systems.
 */
public interface Deserializer<T> {

    public class DeserializerException extends Exception {
        public DeserializerException(String message) {
            super(message);
        }
        public DeserializerException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Serializes the given object.
     */
    public T deserialize(String string) throws DeserializerException;
}
