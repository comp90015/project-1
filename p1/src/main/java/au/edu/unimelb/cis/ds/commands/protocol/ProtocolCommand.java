package au.edu.unimelb.cis.ds.commands.protocol;

import au.edu.unimelb.cis.ds.commands.RRAbstractCommand;

/**
 * Command related to the protocol
 * on the wire itself.
 */
public abstract class ProtocolCommand extends RRAbstractCommand {

    public ProtocolCommand() {
        super();
    }
    // Create no dependencies for base command

}
