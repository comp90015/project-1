package au.edu.unimelb.cis.ds.commands.filesync;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;
import au.edu.unimelb.cis.ds.commands.protocol.Acknowledge;
import au.edu.unimelb.cis.ds.filesync.FileSyncModel;
import filesync.BlockUnavailableException;
import filesync.NewBlockInstruction;

import java.io.IOException;

public class NewBlockCommand extends InstructionCommand {

    private NewBlockInstruction getInstruction() {
        return instruction;
    }

    private void setInstruction(NewBlockInstruction instruction) {
        this.instruction = instruction;
    }

    private NewBlockInstruction instruction;

    private NewBlockCommand() {
        super();
    }

    public NewBlockCommand(NewBlockInstruction instruction) {
        this.instruction = instruction;
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {
        return defaultProcess(instruction, conversationState);
    }
}
