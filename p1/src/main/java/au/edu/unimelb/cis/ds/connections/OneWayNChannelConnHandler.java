package au.edu.unimelb.cis.ds.connections;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * A read/write will block all channels
 * in the connection handler due to its implementation.
 */
public class OneWayNChannelConnHandler implements NChannelConnHandler {
    private final Logger log = Logger.getLogger(this.getClass());

    // Resources
    private Socket connectionSocket;

    // Message inbox
    private CapturedChannelMessagesInbox channelMessagesInbox;

    // Serialization
    private JSONSerializer messageJSONSerializer;
    private JSONDeserializer<ChannelProtocolMessage> messageJSONDeserializer;

    // Currently constants, but easily refactorable to be user-set
    private final String MessageDelimiter = "\r\n";
    private final String IncidentalDelimiterReplace = "\n";

    public OneWayNChannelConnHandler(int numberOfChannels) {

        // Check arguments
        checkArgument(numberOfChannels > 0, "Argument was %s, but expected positive", numberOfChannels);

        // Create captured channel messages inbox
        channelMessagesInbox = new CapturedChannelMessagesInbox(numberOfChannels);

        // Create serialization
        messageJSONSerializer = new JSONSerializer();
        messageJSONDeserializer = new JSONDeserializer<ChannelProtocolMessage>();
    }

    /**
     * Captured messages in order of retrieval
     * grouped by channel
     */
    private class CapturedChannelMessagesInbox {
        private List<List<String>> listOfChannelMessages;

        private CapturedChannelMessagesInbox(int numberOfChannels) {

            // Create list of channel message lists
            listOfChannelMessages = new ArrayList<List<String>>(numberOfChannels);

            // Create message list for each channel
            for (int i = 0; i < numberOfChannels; i++) {
                listOfChannelMessages.add(new LinkedList<String>());
            }
        }

        public void addCapturedMessageForChannel(int channelNumber, String message) {
            listOfChannelMessages.get(channelNumber).add(message);
        }

        public List<String> getCapturedMessagesForChannel(int channelNumber) {
            return listOfChannelMessages.get(channelNumber);
        }
    }

    /**
     * Reads from channel.
     *
     * @param channelNumber which channel
     * @return message
     * @throws IOException
     */
    @Override
    public String read(int channelNumber) throws IOException {

        // Read to correct endpoint channel
        return readMessageFromChannel(channelNumber);

    }

    private String readMessageFromChannel(int channelNumber)
            throws IOException {

        log.debug(String.format("Reading message from channel %d", channelNumber));

        // Check if message exists in inbox
        List<String> channelMessages = channelMessagesInbox.getCapturedMessagesForChannel(channelNumber);
        if (!channelMessages.isEmpty()) {

            // Extract and return oldest message
            String message = channelMessages.remove(0);
            log.debug(String.format("Returning message found in inbox: %s", message));
            return message;

        }

        InputStream inputStream = connectionSocket.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        // Continue forever
        log.debug("Message not found in inbox, reading from socket until found");
        while (true) {

            Scanner channelScanner = null;
            String inputMessage = null;
            try {
                // Read message from channel's input stream
                // using instance delimiter
                channelScanner = new Scanner(inputStream);
                channelScanner.useDelimiter(MessageDelimiter);
                inputMessage = channelScanner.next();
            }
            catch (Exception e) {
                throw new IOException("Scanner could not get next", e);
            }

            // Read message
            ChannelProtocolMessage receivedMessage = messageJSONDeserializer.deserialize(inputMessage);
            log.debug("Received message");
            log.info(receivedMessage.toString());

            // If message received is of the correct channel
            if (receivedMessage.getChannelNumber() == channelNumber) {
                log.debug(String.format(
                        "Message found of correct channel %d",
                        channelNumber
                ));

                // Break out of loop and return message
                return receivedMessage.getMessage();

            }
            // Otherwise
            else {
                log.debug(String.format(
                        "Message found of channel %d, adding to inbox: %s",
                        channelNumber,
                        receivedMessage
                ));

                // Add message to respective channel
                channelMessagesInbox.addCapturedMessageForChannel(
                        receivedMessage.getChannelNumber(),
                        receivedMessage.getMessage()
                );

            }

        }

    }

    /**
     * Writes to channel.
     *
     * @param channelNumber which channel
     * @param message message to send
     * @return self
     * @throws IOException
     */
    @Override
    public NChannelConnHandler write(int channelNumber, String message) throws IOException {

        // Write to correct channel
        writeMessageToChannel(channelNumber, message);

        // Return self
        return this;
    }

    private void writeMessageToChannel(
            int channelNumber, String message)
            throws IOException {

        // Create protocol message
        ChannelProtocolMessage protocolMessage = new ChannelProtocolMessage(channelNumber, message);
        String JSONifiedProtocolMessage = messageJSONSerializer.serialize(protocolMessage);

        // Check if delimiter is in JSONified message
        if (JSONifiedProtocolMessage.contains(MessageDelimiter)) {
            // Replace delimiters in output message
            JSONifiedProtocolMessage = JSONifiedProtocolMessage.replace(
                    MessageDelimiter,
                    IncidentalDelimiterReplace
            );
        }

        // Complete message by adding delimiter
        JSONifiedProtocolMessage += MessageDelimiter;
        log.debug("Created protocol message");

        // Send protocol message over the wire
        log.debug("Sending protocol message");
        log.info(JSONifiedProtocolMessage);
        OutputStream outputStream = connectionSocket.getOutputStream();
        outputStream.write(JSONifiedProtocolMessage.getBytes());
        log.debug("Sent protocol message");
    }

    @Override
    public NChannelConnHandler listenForConnection(InetAddress inetAddress, int portNumber) throws IOException {

        // Start listening for connections to build connection
        ServerSocket listener = new ServerSocket(portNumber, 0, inetAddress);
        log.debug(String.format("Listening at %s:%s", inetAddress, portNumber));
        connectionSocket = listener.accept();
        log.debug(String.format("Connection established with %s"
                , inetAddress));

        // Return self
        return this;
    }

    @Override
    public NChannelConnHandler connect(InetAddress inetAddress, int portNumber) throws IOException {

        // Connect
        log.debug(String.format("Connecting to %s:%s", inetAddress, portNumber));
        connectionSocket = new Socket(inetAddress, portNumber);
        log.debug(String.format("Connection established with %s"
                , inetAddress));

        // Return self
        return this;
    }

    @Override
    public NChannelConnHandler closeConnection() {

        // Close resources
        idempotentCloseSocket(connectionSocket);

        // Return self
        return this;
    }

    private void idempotentCloseSocket(Socket socket) {

        try {

            // If socket still active, close
            if (socket.isConnected()) {
                log.debug("Connection closed");
                socket.close();
            }

        } catch (IOException e) {
            log.error("Network error", e);
        }

    }
}
