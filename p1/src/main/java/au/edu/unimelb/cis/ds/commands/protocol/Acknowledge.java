package au.edu.unimelb.cis.ds.commands.protocol;

import au.edu.unimelb.cis.ds.commands.RRCommand;
import au.edu.unimelb.cis.ds.commands.RRConversationState;

/**
 * Acknowledge command when an RR conversation ends.
 * The command handler is required to identify and handle this
 * appropriately. It is the base case for the
 * recursive data type known as conversations.
 */
public class Acknowledge extends ProtocolCommand {

    public Acknowledge() {
        super();
    }

    @Override
    public RRCommand execute(RRConversationState conversationState) {

        // End conversation
        conversationState.endConversation();

        // Return null, the command execution handler
        // would normally be iterating until
        // no more are provided.
        return null;

    }

}
