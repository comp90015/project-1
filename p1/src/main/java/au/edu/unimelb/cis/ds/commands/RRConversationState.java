package au.edu.unimelb.cis.ds.commands;

import au.edu.unimelb.cis.ds.commands.protocol.Acknowledge;
import au.edu.unimelb.cis.ds.commands.protocol.Disconnect;

import java.util.List;

/**
 * State of conversation
 */
public class RRConversationState {
    private boolean abruptEnding;
    private boolean ended;
    private List<RRCommand> commandHistory;

    public RRConversationState(List<RRCommand> commandHistory) {
        this.abruptEnding = false;
        this.ended = false;
        this.commandHistory = commandHistory;
    }

    /**
     * Returns history of commands sent and received.
     * @return history of commands sent and received
     */
    public List<RRCommand> getCommandHistory() {
        return commandHistory;
    }

    /**
     * Check if conversation ended abruptly
     * @return true if abrupt ending
     */
    public boolean isAbruptEnding() {
        return abruptEnding;
    }

    /**
     * Check if conversation has ended
     * @return true if ended
     */
    public boolean isEnded() {
        return ended;
    }

    /**
     * Ends conversation peacefully
     *
     * @return acknowledge command
     */
    public RRCommand endConversation() {
        ended = true;
        return new Acknowledge();
    }

    /**
     * End the conversation abruptly,
     * without consultation from the peer.
     *
     * @return disconnect command
     */
    public RRCommand endConversationAbruptly() {
        abruptEnding = true;
        endConversation();
        return new Disconnect();
    }

}