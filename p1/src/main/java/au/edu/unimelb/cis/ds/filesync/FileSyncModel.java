package au.edu.unimelb.cis.ds.filesync;

import au.edu.unimelb.cis.ds.monitors.SynchronisedFileMonitor;
import filesync.BlockUnavailableException;
import filesync.Instruction;
import filesync.SynchronisedFile;
import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * File-sync model that handles
 * interactions of a provided file and its changes.
 */
public class FileSyncModel {
    private final Logger log = Logger.getLogger(this.getClass());

    private FileSyncEndPointOptions fileSyncOptions;
    private ThreadSafeSynchronisedFile synchronisedFile;
    private SynchronisedFileMonitor synchronisedFileMonitor;

    /**
     * Creates file sync model.
     *
     * @param fileSyncOptions preferred file sync options
     * @param synchronisedFile synchronised file object
     * @param synchronisedFileMonitor file monitor
     */
    public FileSyncModel(FileSyncEndPointOptions fileSyncOptions,
                         ThreadSafeSynchronisedFile synchronisedFile,
                         SynchronisedFileMonitor synchronisedFileMonitor) {
        this.fileSyncOptions = fileSyncOptions;
        this.synchronisedFile = synchronisedFile;
        this.synchronisedFileMonitor = synchronisedFileMonitor;
    }

    /**
     * Start monitoring file for changes
     */
    public void enableFileMonitoring() {
        log.debug("Enabling file monitor");
        synchronisedFileMonitor.startMonitoringFile();
    }

    /**
     * @see SynchronisedFileMonitor
     */
    public void disableFileMonitoring() {
        throw new NotImplementedException();
    }

    /**
     * Get current endpoint options
     * @return current endpoint options
     */
    public FileSyncEndPointOptions getFileSyncOptions() {
        return fileSyncOptions;
    }

    /**
     * Set current endpoint options
     * @param fileSyncOptions current endpoint options
     */
    public void setFileSyncOptions(FileSyncEndPointOptions fileSyncOptions) {
        this.fileSyncOptions = fileSyncOptions;
    }

    /**
     * Returns next instruction to be processed for file synchronisation.
     * Requires file monitoring to be enabled.
     * @return next instruction to be processed on other endpoint
     */
    public Instruction getNextInstruction() {

        // Retrieve next instruction
        log.debug("Getting next instruction");
        Instruction instruction = synchronisedFile.nextInstruction();
        log.debug("Received next instruction");
        log.trace(instruction);

        // Return next instruction
        return instruction;
    }

    /**
     *
     * Processes instruction sent from other endpoint.
     *
     * @param instruction instruction to process on own endpoint
     * @throws IOException
     * @throws BlockUnavailableException
     */
    public void processInstruction(Instruction instruction) throws IOException, BlockUnavailableException {
        log.debug("Processing instruction");
        log.trace(instruction);
        synchronisedFile.processInstruction(instruction);
    }
}
