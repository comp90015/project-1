# Makefile for Project 1

MVN=mvn
MVN_POM_PATH=p1/pom.xml
MVN_OPTS=-f $(MVN_POM_PATH)

# Rules
.PHONY: build clean rebuild-test

build: clean
	# Build JAR file
	$(MVN) package $(MVN_OPTS)
	cp -f p1/target/p1-1.0-SNAPSHOT.jar syncserver.jar
	cp -f syncserver.jar syncclient.jar

rebuild-test:
	# Rebuilds test by resetting test directory
	-rm -f test/*.tmp
	cp -f test/source.txt test/client.txt
	cp -f test/source.txt test/server.txt

clean: rebuild-test
	# Remove JAR files
	# The '-' at the start, signifies continue regardless of exit code
	-rm -f *.jar
	# Clean maven project
	$(MVN) clean $(MVN_OPTS)
