#!/usr/bin/env bash
#
# Installs JAR files into cis-repo
# in p1 maven project.
#

# Get library directory
# Should be where this script resides
JAR_LIB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Install custom JAR files
mvn install:install-file \
        -Dfile="$JAR_LIB_DIR/filesync(1).jar" \
        -DgroupId=au.edu.unimelb.cis.ds \
        -DartifactId=filesync \
        -Dversion=1.0 \
        -Dpackaging=jar \
        -DlocalRepositoryPath=$(readlink -f ../p1/cis-repo)

mvn install:install-file \
        -Dfile="$JAR_LIB_DIR/commons-codec-1.7.jar" \
        -DgroupId=au.edu.unimelb.cis.ds \
        -DartifactId=commons \
        -Dversion=1.7 \
        -Dpackaging=jar \
        -DlocalRepositoryPath=$(readlink -f ../p1/cis-repo)

mvn install:install-file \
        -Dfile="$JAR_LIB_DIR/json_simple-1.1.jar" \
        -DgroupId=au.edu.unimelb.cis.ds \
        -DartifactId=json_simple \
        -Dversion=1.1 \
        -Dpackaging=jar \
        -DlocalRepositoryPath=$(readlink -f ../p1/cis-repo)
